# an incomplete list; add more to here as we use them to make check work
TIC_GLOBALS=mget,btn,btnp,map,music,spr,rect,rectb,trace,pix,mset,cls,line,sfx,textri,circb,circ,pmem,sync
SRC=bloip.fnl
run: ; tic80 $(SRC)

test: ; fennel test.fnl

bloip.html.zip: bloip.fnl
	tic80 --cmd "load bloip.fnl & save bloip.tic & export html bloip.html & exit"
	mv ~/.local/share/com.nesbox.tic/TIC-80/bloip.html.zip .

bloip.tar.gz: bloip.fnl README.md bloip.tic license.txt
	mkdir -p bloip-bloip
	cp $^ bloip-bloip
	tar cfz $@ bloip-bloip/

upload: bloip.html.zip
	butler push $< technomancy/bloip-bloip:bloip.html.zip

upload-draft: bloip.html.zip
	rm -f draft/*
	unzip $< -d draft
	rsync -rAv draft/* p:p/bloip-bloip/

check: $(SRC) ; fennel --plugin ../fennel/src/linter.fnl --globals $(TIC_GLOBALS) \
		--compile $< > /dev/null
count: $(SRC) ; cloc $^

.PHONY: run test check count upload upload-draft
