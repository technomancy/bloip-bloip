# Bloip Bloip

You are a member of an activist group who is dealing with cleaning up
rivers and lakes from plastic pollution. The bloips can help you break
down plastics if you deploy them with care.

To play locally, download [TIC-80](https://tic80.com) and run `tic80
bloip.tic` in this directory. The pro version of TIC-80 (available for
free if you build from source) can load the `bloip.fnl` text cart as well.

## TODO

* [X] smoother drop
* [X] animate clearing
* [X] animate falling
* [X] chain combos
* [X] plastic
* [X] next bloip indicator
* [X] plastic attacks
* [X] taller level 1
* [X] chain clearing
* [X] over-powerful chain effect
* [X] level progression
* [X] variable field size
* [X] wall kicks
* [X] target sight
* [X] down only drops one piece
* [X] intro
* [X] plastic above doesn't get cleared
* [X] multiplayer
* [X] scoring endless mode
* [X] grouping bug
* [X] plastic can fall thru normal bloips
* [X] colorblind mode
* [X] multiplayer attacks
* [X] credits screen
* [X] credits background
* [X] balance difficulty
* [X] increase endless mode attack
* [X] multiplayer-specific endgame screen
* [ ] increase music tempo when you're close to dying

### animations

* [X] sprite face bug
* [X] bloip bounce animation
* [X] animate rotation
* [ ] animate kick
* [X] win screen
* [-] lose screen
* [X] sprites for joining up bloips
* [X] intro dialog

### sfx

* [X] rotate (pshh)
* [X] move (blip)
* [X] land (bloop)
* [X] clear (shimmer, increase pitch for each level of chain)
* [ ] plastic drop (crunch)

### music
* [X] main theme
* [ ] win screen
* [ ] lose screen
* [ ] intro

## License

Code and art copyright © 2021 Phil Hagelberg, distributed under the GNU
General Public License version 3 or later; see file license.txt.

Music © 2021 Grant Shangreaux, distributed under Creative Commons Share-Alike 4.0
