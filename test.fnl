(local fennel (require :fennel))

(local (w h) (values 5 11))
(local directions [[1 0] [0 1] [-1 0] [0 -1]])

(fn check-clear [get x y out]
  (let [color (get x y)]
    (each [_ [dx dy] (ipairs directions)]
      (let [nx (+ x dx) ny (+ y dy)
            k (.. nx "x" ny)]
        (when (and (<= 0 nx w) (<= 0 ny h)
                   (= color (get nx ny))
                   (not (. out k)))
          (tset out k [nx ny])
          (check-clear get nx ny out)))))
  (let [cleared (icollect [_ v (pairs out)] v)]
    (if (<= 4 (length cleared))
        cleared
        [])))

(fn compact [get set! x]
  (var highest h)
  (fn move [from to]
    (set! x to (get x from))
    (set! x from 0)
    (set highest (- to 1)))
  (while (and (not= 0 (get x highest))
              (not= 0 highest))
    (set highest (- highest 1)))
  (for [y highest 0 -1]
    (when (not= 0 (get x y))
      (move y highest))))

(fn maybe-clear [get set! x y]
  (let [cols []]
    (each [_ [cx cy] (ipairs (check-clear get x y {}))]
      (tset cols cx true)
      (set! cx cy 0))
    (each [col (pairs cols)]
      (compact get set! col))))

(fn sort [tbl]
  (and tbl (table.sort tbl #(< (fennel.view $1) (fennel.view $2)))))

(fn test [x y field result]
  (maybe-clear #(. field (+ $2 1) (+ $1 1))
                 #(tset field (+ $2 1) (+ $1 1) $3) x y)
  (let [expected (fennel.view result)
        actual (fennel.view field)]
    (when (not= expected actual)
      (print "Want:")
      (print expected)
      (print "Got:")
      (print actual)
      (error :fail))))

(test 4 10
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 3 0 0 ]
       [ 0 0 0 4 0 0 ]
       [ 0 0 4 1 1 2 ]
       [ 0 2 4 4 1 1 ]]
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 3 0 0 ]
       [ 0 0 4 4 0 0 ]
       [ 0 2 4 4 0 2 ]])

(test 0 11
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [1 0 0 0 0 0]]
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [1 0 0 0 0 0]])

(test 1 9
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [2 1 0 0 0 0]
       [1 1 1 1 0 0]
       [2 1 0 0 0 0]]
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [2 0 0 0 0 0]
       [2 0 0 0 0 0]])


(test 4 7
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [1 1 1 0 1 0]]
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [1 1 1 0 1 0]])

(test 0 10
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [3 3 3 3 4 0]
       [1 1 1 2 4 0]]
      [[0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 0 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [0 0 0 0 2 0]
       [0 0 0 0 4 0]
       [1 1 1 2 4 0]])

(test 5 9
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 1 0 0 ]
       [ 0 0 2 3 3 3 ]
       [ 0 2 3 3 4 4 ]
       [ 0 2 3 0 1 4 ]]
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 2 0 0 4 4 ]
       [ 0 2 2 1 1 4 ]])

(test 2 9
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 1 0 0 0 0 ]
       [ 0 3 2 1 0 0 ]
       [ 2 3 2 2 0 0 ]
       [ 2 4 1 2 0 0 ]
       [ 2 1 1 2 0 0 ]]
      [[ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 0 0 0 0 0 ]
       [ 0 1 0 0 0 0 ]
       [ 0 3 0 0 0 0 ]
       [ 2 3 0 0 0 0 ]
       [ 2 4 1 0 0 0 ]
       [ 2 1 1 1 0 0 ]])

(print "Success!!!")
