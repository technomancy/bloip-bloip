;; title:  bloip bloip
;; author: technomancy / shoshin
;; desc:   falling blobs
;; script: fennel
;; saveid: bloip-bloip

;;; setup
(var (w h tics-per-move) (values 5 13 30))

(local bounce-time 10)
(local directions [[1 0] [0 1] [-1 0] [0 -1]])
(local levels [{:rows 2 :w 5 :h 13 :attack 512}
               {:rows 3 :w 7 :h 12 :attack 412}
               {:rows 3 :w 7 :h 10 :attack 384}
               {:rows 4 :w 7 :h 9  :attack 256}])

(tset levels 0 {:rows 0 :w 7 :h 13 :attack 200})

(macro icollect [iter-tbl value-expr ...] ; backport from fennel 0.10.0
  `(let [tbl# []]
     (var i# (length tbl#))
     (each ,iter-tbl
       (let [val# ,value-expr]
         (when (not= nil val#)
           (set i# (+ i# 1))
           (tset tbl# i# val#))))
     tbl#))

(macro collect [iter-tbl key-value-expr ...]
  `(let [tbl# []]
     (each ,iter-tbl
       (match ,key-value-expr
         (k# v#) (tset tbl# k# v#)))
     tbl#))

(macro accumulate [iter-tbl accum-expr]
  (let [accum-var (table.remove iter-tbl 1)
        accum-init (table.remove iter-tbl 1)]
    `(do (var ,accum-var ,accum-init)
         (each ,iter-tbl
           (set ,accum-var ,accum-expr))
         ,accum-var)))

;; (fn range [from to ?step]
;;   (let [step (or ?step 1)]
;;     (values (fn [_ l]
;;               (let [n (+ l step)]
;;                 (if (or (and (> step 0) (<= n to))
;;                         (and (< step 0) (>= n to)) (= step 0)) n)))
;;             nil (- from step))))

;; (fn dbg [x y]
;;   (trace (.. :======== x " " y))
;;   (for [y 0 h]
;;     (let [row ["["]]
;;       (for [x 0 w] (table.insert row (mget x y)))
;;       (table.insert row "]")
;;       (trace (table.concat row " "))))
;;   (trace :========))

(local tutorial ["You need to get\nrid of the plastic\nat the bottom."
                 "The falling bloips\ncan clear it."
                 (.. "Put them in groups\n"
                     "of 4 or more where\n"
                     "the colors match.")
                 "Any adjacent plastic\nwill be cleared."
                 "Chain up combos\nfor more power."
                 "As you progress some\nmore plastic will fall."
                 "Do what you can\nto clear it all!"])

(fn prn [...]
  (trace (table.concat (icollect [_ x (ipairs [...])] (tostring x)) " ")))

(fn make-piece []
  {:a (math.random 1 4) :b (math.random 1 4) :x 2 :y 0 :r 1})

(fn plastic? [tile] (< 4 (math.fmod tile 16)))

;; color-aware get
(fn cget [x y] (math.fmod (mget x y) 16))

(local (state1 state2) (values {} {}))

(fn opponent [state] (match state state1 state2 _ state1))

;;; piece positioning

;; all coordinates outside the "draw" functions refer to tile coordinates, not
;; pixel coordinates.

;; every piece (pair) is made up of an "a" bloip and a "b" bloip. the position
;; of the "a" comes from the x/y fields but the position of "b" depends on the
;; rotation; this function returns x and y coords for the "b" bloip.
(fn piece-bxy [{: x : y : r} ?r]
  (let [[dx dy] (. directions (or ?r r))]
    (values (+ dx x) (+ dy y))))

(fn landed-1? [x y]
  (or (= y h) (not= 0 (mget x (+ y 1)))))

(fn landed? [piece]
  (or (landed-1? piece.x piece.y) (landed-1? (piece-bxy piece))))

(fn get-group [get x y group ?min]
  (let [color (get x y)]
    (each [_ [dx dy] (ipairs directions)]
      (let [nx (+ x dx) ny (+ y dy)
            k (.. nx "x" ny)]
        (when (and (<= 0 nx w) (<= 0 ny h)
                   (= color (get nx ny)) (not= 0 color)
                   (not (plastic? color)) (not (. group k)))
          (tset group k [nx ny])
          (get-group get nx ny group ?min)))))
  (let [cleared (icollect [_ v (pairs group)] v)]
    (if (<= (or ?min 4) (length cleared))
        cleared
        [])))

(fn top [col y get] ; from the top
  (if (= y h) h
      (= 0 (get col (+ y 1))) (top col (+ y 1) get)
      y))

(fn compact [get set! x falling]
  (var highest h) ; from the bottom
  (while (and (not= 0 (get x highest))
              (not= 0 highest))
    (set highest (- highest 1)))
  (for [y highest 0 -1]
    (let [color (cget x y)]
      (when (not= 0 color)
        (set! x y 0)
        (table.insert falling [color x y])))))

(fn start-clear-groups [get x y clearing chain]
  (let [groups (get-group get x y {} 4)]
    (each [_ [cx cy] (ipairs groups)]
      (table.insert clearing [(get cx cy) cx cy 10 (* 2 chain)]))
    (not= 0 (length groups))))

(fn endless-attack [state]
  (set state.attack-power (+ 1 (math.floor (math.log (// state.t 1024)))))
  (set state.attack-timer (* state.attack-max state.attack-power)))

(fn attack [state]
  (if (= state.attack-power 1)
      (table.insert state.falling [5 state.attack-col 0 true])
      (for [row 0 (- state.attack-power 2)]
        (for [x 0 w]
          (table.insert state.falling [5 x row true]))))
  (if state.endless?
      (endless-attack state)
      (set (state.attack-timer state.attack-power) (values state.attack-max 1)))
  (set state.attack-col (math.random w)))

(fn down [state]
  (when state.piece
    (when (not (landed? state.piece))
      (set state.piece.y (+ state.piece.y 1)))
    (when (landed? state.piece)
      (set state.falling [[state.piece.b (piece-bxy state.piece)]
                          [state.piece.a state.piece.x state.piece.y]])
      (set (state.piece state.rt) nil)
      (when (< state.attack-timer 0)
        (attack state)))))

(fn rotate [state piece dir]
  (when state.piece
    (set piece.r (+ (math.fmod piece.r 4) dir))
    (set state.rt 3)
    (let [(bx by) (piece-bxy piece)]
      ;; did we rotate off-screen?
      (when (or (not= 0 (mget bx by)) (not (<= 0 bx w)))
        ;; if so either wall-kick (if there's room) or double-rotate.
        (let [(bx2 by2) (piece-bxy piece (if (= piece.r 3) 1 3))]
          (if (and (= 0 (mget bx2 by2)) (<= 0 bx2 w))
              (set (piece.x piece.y) (values bx2 by2)) ; kick
              (rotate state piece dir)))))))

(fn move [piece dir]
  (when piece
    (let [min-x (if (= piece.r 3) 1 0)
          max-x (if (= piece.r 1) (- w 1) w)
          target (match (values piece.r dir)
                   (1 1) (+ piece.x 2)
                   (3 -1) (- piece.x 2)
                   _ (+ piece.x dir))
          clear? (= 0 (mget target piece.y))]
      (set piece.mt dir)
      (when clear?
        (set piece.x (-> (+ piece.x dir) (math.min max-x) (math.max min-x)))))))

(var last-down nil)
(fn input [state]
  (when (= 0 (length state.falling))
    (when (and (= 0 (math.fmod state.t (// tics-per-move 10))) (btn 1)
               (or (= state.piece last-down) (btnp 1)))
      (down state))
    (when (btnp 1) (set last-down state.piece))
    (when (btnp 0) (rotate state state.piece 1) (sfx 7))
    (when (btnp 2) (move state.piece -1) (sfx 6 "G-5"))
    (when (btnp 3) (move state.piece 1) (sfx 6 "G-5"))))

(fn win? [state]
  (set state.plastic-gone? true)
  (for [x 0 w]
    (for [y 0 h]
      (when (plastic? (mget x y))
        (set state.plastic-gone? false))))
  (each [_ [color] (ipairs state.falling)]
    (when (plastic? color)
      (set state.plastic-gone? false)))
  state.plastic-gone?)

(fn init-plastics [rows]
  (for [yi 0 (- rows 1)]
    (for [x 0 w]
      (mset x (- h yi) 5)))
  (for [x 0 w]
    (when (< 0.5 (math.random))
      (mset x (- h rows) 5))))

(fn endgame [text allow-retry? retry]
  (cls)
  (print text 32 52 9 false 3)
  (print text 30 50 1 false 3)
  (when allow-retry?
    (print "press Z to retry" 64 108 2)
    (print "or any other key to restart" 64 118 2)
    (if (btnp 4)
        (set _G.TIC retry)
        (for [i 0 7]
          (when (btnp i)
            (set _G.TIC _G.menu)))))
  (when (not allow-retry?)
    (print "press an arrow key" 64 118 2)
    (for [i 0 7]
      (when (btnp i)
        (set _G.TIC _G.menu)))))

(fn init [play state endless?]
  (when (and (not (= _G.soundtrack "game")) (= 0 (pmem 0)))
    (music 0) (set _G.soundtrack "game"))
  (set (state.piece state.next-piece) (values (make-piece) (make-piece)))
  (set (state.falling state.clearing state.chain) (values [] [] 0))
  (set state.tutorial (if (= state.level 1)
                          (icollect [_ l (ipairs tutorial)] l)
                          []))
  (set (state.ox state.oy state.score) (values 64 16 0))
  (set (state.bounce state.endless?) (values [] endless?))
  (for [x 0 w] (for [y 0 h] (mset x y 0)))
  (set _G.TIC play)
  (match (. levels state.level)
    level (do
            (set (w h state.attack-max) (values level.w level.h level.attack))
            (init-plastics level.rows)))
  (when endless?
    (init-plastics 5))
  (set state.t 0)
  (set state.attack-col (math.random w))
  (set (state.attack-timer state.attack-power) (values state.attack-max 1)))

(fn all-clear? []
  (for [x 0 w]
    (for [y 0 h]
      (when (not= 0 (mget x y))
        (lua "return false"))))
  true)

(fn multiplayer? [state]
  (and (not state.endless?) (= state.level 0)))

(fn update [state dialog]
  (down state)
  (when (and (not= state.level 0) (win? state))
    (let [next-level (+ state.level 1)]
      (set _G.TIC (partial dialog next-level))))
  (when (not= 0 (mget 2 0))
    (if (= state state2) (set _G.TIC (partial endgame "Player 1 wins"))
        (and (= state state1)
             (multiplayer? state))
        (set _G.TIC (partial endgame "Player 2 wins"))
        (set _G.TIC (partial endgame "GAME OVER" (not= 0 state.level)
                             (partial dialog state.level))))
    (when (= 0 (pmem 0))
      (music 2 0 0 false))))

(fn matching-neighbors [color x y]
  (icollect [_ [dx dy] (ipairs directions)]
    (and (= color (cget (+ dx x) (+ dy y))) [(+ dx x) (+ dy y)])))

(fn connecting-tile [color matches]
  (+ color (* 16 (match matches
                   [false false false false] 0
                   [[] false false false] 1
                   [false [] false false] 2
                   [false false [] false] 3
                   [false false false []] 4
                   [[] [] false false] 5
                   [[] false [] false] 6
                   [[] false false []] 7
                   [false [] [] false] 8
                   [false [] false []] 9
                   [false false [] []] 10
                   [false [] [] []] 11
                   [[] false [] []] 12
                   [[] [] false []] 13
                   [[] [] [] false] 14
                   _ 0))))

(fn connect-group [x y]
  (let [color (cget x y)]
    (when (not (plastic? color))
      (each [_ [cx cy] (ipairs (get-group cget x y [] 1))]
        (let [matches (matching-neighbors color cx cy)
              tile (connecting-tile color matches)]
          (mset cx cy tile))))))

(fn land [state i [color x y]]
  (table.remove state.falling i)
  (mset x y color) ; initial set just in order to calculate group
  (sfx 9)
  (table.insert state.landed [color x y])
  (table.insert state.bounce [color x y bounce-time]))

(fn fall [state i faller]
  (let [[_ x y attack?] faller]
    (if (landed-1? x y)
        (land state i faller)
        (tset faller 3 (+ y (/ 1 (if attack? 4 8)))))))

(fn clear [state i clearee]
  (let [[tile x y count chain] clearee
        cols {x true}]
    (tset clearee 4 (- count 0.5))
    (when (= 0 (. clearee 4))
      (set state.score (+ state.score 1))
      (table.remove state.clearing i)
      (each [_ [dx dy] (ipairs directions)]
        (let [neighbor (cget (+ x dx) (+ y dy))]
          (when (and (plastic? neighbor) (not= 0 tile)
                     (or (not (plastic? tile)) (< 1 chain)))
            (set state.score (+ state.score chain))
            (mset (+ x dx) (+ y dy) 0)
            (sfx 8 (+ 47 (* state.chain 2)))
            (tset cols (+ x dx) true)
            (table.insert state.clearing [neighbor (+ x dx) (+ y dy) 5
                                          (- chain 1)]))))
      (mset x y 0))))

(fn every-update [state]
  (let [start-chain state.chain]
    (table.sort state.falling (fn [[_ _ y1] [_ _ y2]] (< y1 y2)))
    (for [i (length state.clearing) 1 -1]
      (clear state i (. state.clearing i)))
    (for [col 0 w]
      (compact mget mset col state.falling))
    (when (= 0 (length state.clearing))
      (for [i (length state.falling) 1 -1]
        (fall state i (. state.falling i)))
      (each [_ [tile x y] (ipairs (or state.landed []))]
        (when (and (not (plastic? tile))
                   (start-clear-groups cget x y state.clearing (+ start-chain 1)))
          (set state.chain (+ start-chain 1))
          (set state.attack-timer (math.min (* (math.max state.attack-max
                                                         state.attack-timer)
                                               (+ 1 state.chain))
                                            (* state.attack-max 4)))
          (set state.attack-power
               (math.max 1 (math.floor (* state.attack-power 0.75))))))
      (set state.landed [])))
  (when (and (not state.piece)
             (= 0 (length state.falling))
             (= 0 (length state.clearing)))
    (when (all-clear?)
      (set state.score (+ state.score 100)))
    (for [x 0 w]
      (for [y 0 h]
        (connect-group x y)))
    (let [other (opponent state)]
      (if (= 1 state.chain)
          (set other.attack-timer 0)
          (set other.attack-power (+ (or other.attack-power 1) state.chain))))
    (set state.chain 0)
    (set state.piece state.next-piece)
    (set state.next-piece (make-piece))
    (table.remove state.tutorial 1))
  (set state.attack-timer (- state.attack-timer 1)))

(fn clear-spr [count] (* 16 (- 6 (// count 2))))

(fn draw-border [ox oy width]
  (let [ow (+ 2 width) iw (+ 1 width)]
    (rect (- ox ow) (- oy ow) (+ ow ow 8 (* w 8)) (+ ow ow 8 (* h 8)) 8)
    (rectb (- ox iw) (- oy iw) (+ iw iw 8 (* w 8)) (+ iw iw 8 (* h 8)) 14)))

(fn draw-attack [ox oy col timer attack-max attack-power]
  (let [progress (/ timer attack-max)
        s (-> (+ 192 (* 16 (math.floor (* progress 4))))
              (math.max 192)
              (math.min 240))]
    (spr s (+ ox (* col 8)) (- oy 12) 0)
    (when (< 1 attack-power)
      (print attack-power (+ ox 2 (* col 8)) (- oy 11)))))

(fn sight-rows [ax bx r]
  (let [a-down (top ax 0 mget)
        b-down (top bx 0 mget)]
    (if (= r 2) (values (- a-down 1) b-down)
        (= r 4) (values a-down (- b-down 1))
        (values a-down b-down))))

(fn draw-sight [piece ox oy]
  (let [{: x : a : b : r} piece
        bx (piece-bxy piece)
        (row-a row-b) (sight-rows x bx r)]
    (circb (+ ox 3 (* x 8)) (+ oy 3 (* row-a 8)) 2 a)
    (circb (+ ox 3 (* bx 8)) (+ oy 3 (* row-b 8)) 2 b)))

(fn highest-bounces [bounces]
  (accumulate [h {} _ [_ x y] (ipairs bounces)]
    (if (or (= nil (. h x)) (< y (. h x)))
        (doto h (tset x y))
        h)))

(fn draw-bounce [bounce ox oy highest]
  (let [[color x y timer] bounce]
    (tset bounce 4 (- timer 1))
    (when (and (not (plastic? color)) (= y (. highest x)))
      (spr (+ color 240) (+ ox (* x 8)) (+ oy (* y 8))))
    (= 0 timer)))

(local qtau (/ math.pi 2))
(fn rotating [state x y ax ay ox oy]
  (if (or (= 0 state.rt) (= nil state.rt)) (values x y)
      (let [th (match state.piece.r ; I know, I know, math.atan2, but no.
                 1 (* math.pi 2)
                 2 qtau
                 3 (* math.pi -1)
                 4 (+ math.pi qtau))
            th2 (+ th (* qtau (/ state.rt -4)))]
        (set state.rt (- state.rt 0.25))
        (values (+ ox (* 8 (+ ax (math.cos th2))))
                (+ oy (* 8 (+ ay (math.sin th2))))))))

(fn sliding [piece ox]
  (if (or (= 0 piece.mt) (= nil piece.mt)) ox
      (do
        (set piece.mt (+ piece.mt (if (< 0 piece.mt) -0.25 0.25)))
        (+ ox (* piece.mt -8)))))

(fn draw [state]
  (let [{: piece : chain : attack-col : attack-timer : attack-max : ox : oy} state]
    (draw-border ox oy (+ 1 (// chain 1)))
    (map 0 0 (+ w 1) (+ h 1) ox oy)
    (each [_ [tile x y] (ipairs state.falling)]
      (spr tile (+ ox (* x 8)) (+ oy (* y 8))))
    (for [i (length state.bounce) 1 -1]
      (when (draw-bounce (. state.bounce i) ox oy (highest-bounces state.bounce))
        (table.remove state.bounce i)))
    (each [_ [tile x y count] (ipairs state.clearing)]
      (when (not= 0 tile)
        (spr (clear-spr count) (+ ox (* x 8)) (+ oy (* y 8)))
        (when (not (plastic? tile))
          (print state.chain (+ ox 2 (* x 8)) (+ oy 1 (* y 8))))))
    (when piece
      (let [poy (+ oy (math.floor (* 8 (/ (math.fmod state.t tics-per-move)
                                          tics-per-move))))
            pox (sliding state.piece ox)]
        (let [(bx by) (piece-bxy piece)]
          (spr piece.b (rotating state (+ pox (* bx 8)) (+ poy (* by 8))
                                 piece.x piece.y pox poy)))
        (spr piece.a (+ pox (* piece.x 8)) (+ poy (* piece.y 8)) 0)
        (draw-sight piece pox oy)))
    (when (. state.tutorial 1)
      (print (. state.tutorial 1) 126 84))
    (draw-attack ox oy attack-col attack-timer attack-max state.attack-power)
    (when (= 0 state.level)
      (print (.. "Score:\n\n  " state.score) (+ ox (* w 8) 18) (+ oy 20)))
    ;; next
    (rect (+ ox (* w 8) 18) oy 20 12 8)
    (spr state.next-piece.a (+ ox (* w 8) 20) (+ oy 2) 0)
    (spr state.next-piece.b (+ ox (* w 8) 28) (+ oy 2) 0)))

(var credits-scroll 0)
(local credits-text
"Now that you can defend yourselves
you and your team head back to the
river. You resolve to protect the
waters that have given life to all
the beings that depend upon it.




Code by Phil Hagelberg
Music and SFX by Grant Shangreaux
Sprites by Noah and Zach Hagelberg

This game was created on land which
was taken from the Nisqually and
Steh-chass people by means of
violence and fraud and must be
returned to the people who belong
to it: those whose ancestors
stewarded it for their relatives
living today who continue the work
for their children to come.

If you live somewhere with a
similar history, please research
the events that led to its
colonization and exploitation
and consider what you can do
in light of that.

Decolonization is not a metaphor.")

(fn credits []
  (cls)
  (set credits-scroll (+ credits-scroll 0.1))
  (map 0 18)
  (print credits-text 45 (- 141 (// credits-scroll 1)) 0)
  (print credits-text 44 (- 140 (// credits-scroll 1)))
  (when (btnp 4)
    (if (< 250 credits-scroll)
        (set _G.TIC _G.menu)
        (set credits-scroll (+ credits-scroll 64))))
  (when (< 350 credits-scroll) (set _G.TIC _G.menu))
  (when (btnp 0) (set credits-scroll (- credits-scroll 32)))
  (when (btnp 1) (set credits-scroll (+ credits-scroll 32))))

(local dialogs [[[256 "glad you could come help us
out. as you know we're all very
excited about the possibilities
of these micro-organisms."]
                 [256 "their remarkable ability to break
down the polymers in plastic waste
has amazing possibilities."]

                 [256 "do you have any experience\nworking with them?"]
                 [320 "I... could use a refresher."]
                 [256 "here, try this."]]

                [[256 "now to take it to the real world."]
                 [256 "there's a pond down the street
that's gotten a lot of plastic
dumped into it; let's see what you
can do there with some bloips."]
                 [320 "skoden!"]]
                [[256 "you're really getting the hang of
this. time for a bigger task."]
                [256 "we're gonna take this down to the
Nisqually river right downstream of
a Star-Mart warehouse that's been
dumping loads of their trash."]
                [256 "they're probably not going to be
too happy about it after they
chased us away last time. but this
is our river!"]
                 [320 "it'll be just like when the cops
beat the shit out of my
grandfather for fishing in
that river when he was a kid!"]
                 [256 "well, hopefully not, but we'll see."]]
                [[256 "hey did you order any packages?"]
                 [320 "I don't think so?"]
                 [256 "because that looks like an awful
lot of delivery drones coming our\nway."]
                 [256 "hang on those are armed
drones--they're trying to destroy
the lab!"]
                 [256 "try to use an aerial deployment of
bloips to break down the plastic in
the drones; that could buy us
enough time to save the research\n
in the lab."]]])

(var this-dialog [])

(fn dialog [level]
  (when (and (not (= _G.soundtrack "intro")) (= 0 (pmem 0)))
    (music 1) (set _G.soundtrack "intro"))
  (when (not (. levels level))
    (set (_G.TIC credits-scroll) (values credits 0)))
  (when (. levels level)
    (when (not= level state1.level)
      (set state1.dialog 1)
      (set state1.level level)
      (set this-dialog (icollect [_ l (ipairs (. dialogs level))] l)))
    (when (btnp 4)
      (set state1.dialog (+ state1.dialog 1)))
    (match (. this-dialog state1.dialog)
      nil (init _G.play state1)
      [sprite text] (do (cls)
                        (spr sprite 8 8 -1 1 0 0 4 4)
                        (print text 48 32)))))

(fn _G.play [p2?]
  (when (not p2?) (cls))
  (let [state (if p2? state2 state1)]
    (set state.t (+ state.t 1))
    (input state)
    (when (= 0 (math.fmod state.t tics-per-move))
      (update state dialog))
    (every-update state)
    (draw state))
  prn)

(local p1fns [_G.mget _G.mset _G.btnp _G.btn _G.map])

;; player 2 has a map that's offset by 100 and a button map offset by 8
(local [omget omset obtnp obtn omap] p1fns)
(fn p2mget [x y] (omget (+ x 100) y))
(fn p2mset [x y tile] (omset (+ x 100) y tile))
(fn p2btnp [b] (obtnp (+ b 8)))
(fn p2btn [b] (obtn (+ b 8)))
(fn p2map [x y w h ox oy] (omap (+ x 100) y w h ox oy))

(fn multi []
  ;; just re-use all the same code but with new handler functions!
  (set (_G.mget _G.mset _G.btnp _G.btn _G.map) (table.unpack p1fns))
  (_G.play)
  (set (_G.mget _G.mset _G.btnp _G.btn _G.map) (values p2mget p2mset p2btnp p2btn p2map))
  (_G.play true))

(fn start-multi []
  (set (state1.level state2.level) (values 0 0))
  (init _G.play state1)
  (init _G.play state2)
  (for [x 0 w]
    (for [y 0 h]
      (mset x y 0)
      (p2mset x y 0)))
  (set (state1.ox state1.oy) (values 6 16))
  (set (state2.ox state2.oy) (values 126 16))
  (set _G.TIC multi))

(local actual-sfx sfx)
(fn mute []
  (if (= _G.sfx actual-sfx)
      (do (set _G.sfx #nil)
          (music)
          (pmem 0 1))
      (do (set _G.sfx actual-sfx)
          (music 1 0 0 true)
          (pmem 0 0))))

(fn cb []
  (if (= 0 (pmem 1))
      (do (pmem 1 1)
          (sync 1 1))
      (do (pmem 1 0)
          (sync 1 0))))

(local menu-items [["Mission mode" #(set _G.TIC (partial dialog 1))]
                   ["Endless mode" #(do (set state1.level 0)
                                        (init _G.play state1 true))]
                   ["Multiplayer" start-multi]
                   ["Toggle sound" mute]
                   ["Toggle colorblind" cb]])

(var (menu-select lo) (values 1 0))
(local lines [])
(for [_ 1 12] (table.insert lines (math.random -168 168)))

(fn _G.menu []
  (set (_G.mget _G.mset _G.btnp _G.btn _G.map) (table.unpack p1fns))
  (cls)
  (when (and (not= "intro" _G.soundtrack) (= 0 (pmem 0)))
    (music 1) (set _G.soundtrack "intro"))
  (set lo (+ lo 0.01))
  (each [_ l (ipairs lines)]
    (let [y (* (+ (math.sin lo) 1) 36)]
      (line 0 (+ l 0 y) 240 (+ l 36 (* y 0.5)) 1)
      (line 0 (+ l 1 y) 240 (+ l 37 (* y 0.5)) 1)
      (line 0 (+ l 2 y) 240 (+ l 38 (* y 0.5)) 1)
      (line 0 (+ l 3 (* y 0.5)) 240 (+ l 39 y) 3)
      (line 0 (+ l 4 (* y 0.5)) 240 (+ l 40 y) 3)
      (line 0 (+ l 8 (* y 0.4)) 240 (+ l 45 y) 11)
      (line 0 (+ l 9 (* y 0.4)) 240 (+ l 46 y) 11)))

  (print "BLOIP BLOIP" 28 25 13 false 3)
  (print "BLOIP BLOIP" 26 23 2 false 3)
  (rect 60 60 116 72 8)
  (rect 62 62 112 68 0)
  (each [i [label] (ipairs menu-items)]
    (print label 77 (+ 58 (* i 12)))
    (when (= i menu-select)
      (spr 1 66 (+ 56 (* i 12)) 0)))
  (when (btnp 0) (set menu-select (-> menu-select (- 1)
                                      (math.min (length menu-items))
                                      (math.max 1))))
  (when (btnp 1) (set menu-select (-> menu-select (+ 1)
                                      (math.min (length menu-items))
                                      (math.max 1))))
  (when (btnp 4) ((. menu-items menu-select 2)))
  (print "press Z to select" 178 130 15 false 1 true))

(set _G.TIC _G.menu)

(set _G.soundtrack false)

(when (= 1 (pmem 1))
  (sync 1 1)) ; colorblind tiles

(if (= 0 (pmem 0))
    (music 1 0 0 true)
    (set _G.sfx #nil))

(global s1 state1) ; for repl access
(global s2 state2)

;; <TILES>
;; 001:0011110001f111101f1111111111111111111111111111910111191000111100
;; 002:0022220002f222202f2222222222222222222222222222a202222a2000222200
;; 003:0033330003f333303f3333333333333333333333333333b303333b3000333300
;; 004:0044440004f444404f4444444444444444444444444444c404444c4000444400
;; 005:0077770007ff00707f0000077f00000770000087700000870700887000777700
;; 016:0000000000077000007007000700007007000070007007000007700000000000
;; 017:0011110001ff11111f1111111111111111111111111111990111191100111100
;; 018:0022220002ff22222f2222222222222222222222222222aa02222a2200222200
;; 019:0033330003ff33333f3333333333333333333333333333bb03333b3300333300
;; 020:0044440004ff44444f4444444444444444444444444444cc04444c4400444400
;; 027:eeeeeeeeeeeeeeee8eeeeeee88eeeeee8888eeee8008888e8800088888888088
;; 028:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
;; 029:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0e8eeee0088eee808888ee0888888e
;; 030:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88eeee88888ee8888888e
;; 031:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8eeeeeee8888eeee
;; 032:000770000070070007000070700ff007700ff007070000700070070000077000
;; 033:0011110001f111101f1111111f11111111111111111111111111111191111119
;; 034:0022220002f222202f2222222f222222222222222222222222222222a222222a
;; 035:0033330003f333303f3333333f333333333333333333333333333333b333333b
;; 036:0044440004f444404f4444444f444444444444444444444444444444c444444c
;; 043:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8eeeee888eee88888ee888888
;; 044:eeeee888eeee8888ee888880e888800088800000880000088000088888888888
;; 045:0888088e0808088e8008008e0088808e08888008888888088888888888888888
;; 046:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8888e888880088800008
;; 047:eeeeeeeeeeeeeeeeeee88eeeee88888e8880088e800088880888888888888888
;; 048:0077770007000070700ff00770f00f0770f00f07700ff0070700007000777700
;; 049:0011110011f11110ff1111111111111111111111111111911111991000111100
;; 050:0022220022f22220ff2222222222222222222222222222a22222aa2000222200
;; 051:0033330033f33330ff3333333333333333333333333333b33333bb3000333300
;; 052:0044440044f44440ff4444444444444444444444444444c44444cc4000444400
;; 056:0009999999991111911116611166611666611191119999991111199900099999
;; 057:99999999999699ee99611e119111911111bb19119bbb1119b999999999999900
;; 058:fffffeeeffffffffffffffffffffffffffffffffffffffff66666fffffffffff
;; 059:eeeeeeeefeeeeeeeffffeeeefffffffefffffff6fffffffffffffff6fffffff6
;; 060:eeeeeeeeeeeeeeeee00eee00eee0e0eeeeee0eeeeeeeeeeeeeeeeeeeeeeeeeee
;; 061:eeeeeeeeeeeeeeeeeeeeffffefffffffee6fffffeee6666eeeeeeeeeeeeeeeee
;; 062:eeeeeeeeeeeeeeeeeeffffffffffffffffffffff66ffffffeee6666eeeeeeeee
;; 063:eeeeeeeeeeeeeeeeffffeefe666fffffffffffffffee666eeeeeeeeeeeeeeeee
;; 064:00ffff000f0000f0f0ffff0ff0f00f0ff0f00f0ff0ffff0f0f0000f000ffff00
;; 065:01f1111001f111101f1111111111111111111191111111910111191000111100
;; 066:02f2222002f222202f22222222222222222222a2222222a202222a2000222200
;; 067:03f3333003f333303f33333333333333333333b3333333b303333b3000333300
;; 068:04f4444004f444404f44444444444444444444c4444444c404444c4000444400
;; 074:ffffffffffffffffffffffffffffffffeee6666feeeeee66eeeeeeeeeeeeeeee
;; 075:fffffff6fffffff6fffffff6fffffff6ffffff666666666eeeeeeeeeeeeeeeee
;; 076:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 077:eeeeeeeeeeeeeeeeeeeeeeefeeeeffffee6fffffeeffffffefffffffffffffff
;; 078:eeeeeeeeeeeeeeeefffffeeeffffffeeffffff6effffff66ffffffffffffffff
;; 079:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee66eeeeeef66eeeee
;; 080:ff0000fff000000f00000000000000000000000000000000f000000fff0000ff
;; 081:0011110001ff11111f1111111111111111111111111111910111191101111910
;; 082:0022220002ff22222f2222222222222222222222222222a202222a2202222a20
;; 083:0033330003ff33333f3333333333333333333333333333b303333b3303333b30
;; 084:0044440004ff44444f4444444444444444444444444444c404444c4404444c40
;; 088:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 089:eeeeeeeeeeeeeeeeeeffffefeeffffffee6fffffeee66fffeeeee6ffeeeee66f
;; 090:eeeeeeeefeeeeeeeffeeeeeefffeeeeeffffeeeeffffeeeeffff6eeeff66eeee
;; 091:eeeeeeeeeeeeeeeeeeeeeeeeeeeeefffeeeeffffeeffffffeffffff6effffffe
;; 092:eeeeeeffeeefffffeeffffffeeffffffeeffffffeeffffffee6fffffee666fff
;; 093:ffffffffffffffffffffffffffffffffffffffffffffffffffffff66ff666666
;; 094:ffffffffffffffffffffffffff6fffffff6fffff66ffffff6fffffffffffffff
;; 095:ff66f6eefff6ffeeffffffeefffffffefffffffefffffffefffffffefffffff6
;; 097:0001100011111111fff111111111111111111111111119991111111100011000
;; 098:0002200022222222fff22222222222222222222222222aaa2222222200022000
;; 099:0003300033333333fff33333333333333333333333333bbb3333333300033000
;; 100:0004400044444444fff44444444444444444444444444ccc4444444400044000
;; 104:eeeeeeffeeeeefffeeefffffeeffffffee6ffff6eee6666eeeeeeeeeeeeeeeee
;; 105:ffeeeee6feeeeeeefeeeeeeefeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 106:66eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 107:e6fff6eeee666eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 108:eeee6666eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 109:66ffff66eeeeef6feeeee66feeeeee66eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 110:ffffffffffffffffffffffff6fffffff66666666eeeeeeeeeeeeeeeeeeeeeeee
;; 111:ffffff66fffff6eeffff66eeffff6eee6666eeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 113:01f1111001f111111f1111111111111111111191111111910111191100111100
;; 114:02f2222002f222222f22222222222222222222a2222222a202222a2200222200
;; 115:03f3333003f333333f33333333333333333333b3333333b303333b3300333300
;; 116:04f4444004f444444f44444444444444444444c4444444c404444c4400444400
;; 120:9999999999991111911116611166666666611191119999991111199999199999
;; 121:9999999999979977997117119111911111551911955511195999999999999999
;; 122:00000000888888889999999911111111eeeeeeeee6e6e6e66e666e6e91996999
;; 123:667777766666666666667676666ee76761116ee76616666e6666676667777666
;; 124:eee11111911eeeee8991e11e88899999988888889988889899999989e1ee11ee
;; 125:eeeeeeee66666666116111161111111199999999919999999999999999999999
;; 126:eeeeeeee66666666111161111111111199999999999999999919999999991999
;; 127:eeeeeeee666eee66111666611111111199999999999199999999999999999999
;; 129:0011110011f11110ff1111111111111111111111111111911111191001111910
;; 130:0022220022f22220ff2222222222222222222222222222a222222a2002222a20
;; 131:0033330033f33330ff3333333333333333333333333333b333333b3003333b30
;; 132:0044440044f44440ff4444444444444444444444444444c444444c4004444c40
;; 136:66dddcd666cddcd666cdccd666cdcdd666ddddd666ddddd666cdcdd666ddddd6
;; 137:9991999909999999000991960000099900000099000000000000000000000000
;; 138:0999000099999999119991119991199991119919999999909900000000000000
;; 139:0000000099990000999999999119991199999999000991190000099900000000
;; 140:0000000000009999999999119111999999991199999999999099911900000000
;; 141:0000000099000000999990009199999099199999119919999999191999991919
;; 142:0000000000000000999000009999900019999900919199009919919009199190
;; 143:8888888866666666111111111111111199999999919999999999999999999999
;; 145:01f1111001f1111001f111101111111111111111011119100111191001111910
;; 146:02f2222002f2222002f22220222222222222222202222a2002222a2002222a20
;; 147:03f3333003f3333003f33330333333333333333303333b3003333b3003333b30
;; 148:04f4444004f4444004f44440444444444444444404444c4004444c4004444c40
;; 155:9991990099199900991999009199900091990000999000006990000099000000
;; 156:9999990099199900991999009919900091190000999900009999900091999000
;; 157:0009999900099999009199990919991999999919991999990919999909999999
;; 158:0991999909919199099191990999999900999999001919990099199900099999
;; 159:8888888866666666011111110011111100099999000099990000099900000999
;; 161:011111101ff11110ff1111111111111111111111111111911111191000111100
;; 162:022222202ff22220ff2222222222222222222222222222a222222a2000222200
;; 163:033333303ff33330ff3333333333333333333333333333b333333b3000333300
;; 164:044444404ff44440ff4444444444444444444444444444c444444c4000444400
;; 167:000bbb33191bbb3313bbb3339bbbb3339bbb333bbbb3333b9b33bbbb9b333bdd
;; 168:3b5b3b803b35b389b3b5bb39b3355b31bb33533933b3bb8133b3b89933bbb899
;; 172:9199990091999900999199009991900099919000999190009991990099999900
;; 173:0191999001919990099199110999919900999919000999990000009900000000
;; 174:0009999900991999009919990099199900199999009199990009991900099919
;; 175:9999999999999999999999999999999999999999999999999999999999999999
;; 177:01f1111011f11110ff1111101111111011111110111199101111191001111110
;; 178:02f2222022f22220ff22222022222220222222202222aa2022222a2002222220
;; 179:03f3333033f33330ff33333033333330333333303333bb3033333b3003333330
;; 180:04f4444044f44440ff44444044444440444444404444cc4044444c4004444440
;; 183:3bb5bbb833b55b8bb3bb5b83b33b883bbbbb83bb000883b500883b5b0083bb55
;; 184:000000b0000000bb000000b300000b3b000003bb000033b5000b3b5b0003bb55
;; 185:bb800000bb880000bbb8000033380000533b8000553b88085b3bb808553b388b
;; 186:00088800008bb800008bb88008bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 187:9999919999919999999999999119991199999999000991190000099900000000
;; 188:0009999999999999999999119111999999991199999999999099911900000000
;; 189:99999999999999e99999e9999199999999199999119919999999191900091919
;; 190:0000000000000000999000009999900019999900919199009919919009199190
;; 191:99919999991199999999999999999999999999999991e99999e1999999999999
;; 192:0077770007ff00707f0000077f00000770000087700000870700887000777700
;; 193:01f1111011f11111ff1111111111111111111111111199911111111100000000
;; 194:02f2222022f22222ff22222222222222222222222222aaa22222222200000000
;; 195:03f3333033f33333ff33333333333333333333333333bbb33333333300000000
;; 196:04f4444044f44444ff44444444444444444444444444ccc44444444400000000
;; 199:cdd88800dc8bb800dd8bb880d8bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 200:0003b555003355b30b3b55b30bb55b3305555b33055bb33300bbbb3300b3bb3b
;; 201:3b5b3b8b3b35b388b3b5bb38b3355b38bb33533833b3bb8833b3b8bb33bbb8bb
;; 202:b3bb5b88b3b55b88bbb5b5b88bb5b5b8bb5bb5b8bb5bb5b8b553bb58b5335b58
;; 203:0999000099999999119991119991199991119919999999909900000000000000
;; 204:0000000000000000000000000000000090000000999900009119900099999000
;; 205:0009999900099999009199990919991999999919991999990919999009999990
;; 206:9991999999919190699191906999990099999900991919009999199099999999
;; 207:999999999999119991119999999999999999999999999999999991999e999999
;; 208:00000000000ff00000f00f000f0700f00f0000f000f00f00000ff00000000000
;; 209:0111111001f11111011f11110111111101111111011111910111191101111110
;; 210:0222222002f22222022f22220222222202222222022222a202222a2202222220
;; 211:0333333003f33333033f33330333333303333333033333b303333b3303333330
;; 212:0444444004f44444044f44440444444404444444044444c404444c4404444440
;; 215:3bb5bbb833b55b8bb3bb5b83b33b883bbbbb83bb3bb883b53d883b5bdc83bb55
;; 216:000bbb33000bbb3303bbb3330bbbb3330bbb333bbbb3333b0b33bbbb0b333bdd
;; 217:333bbb803b35bbb85b3355b855b335b833bb355b3bbb3358bbbb3338cdd55338
;; 218:00ddc0000dcdd0000ccddd000cddcc0000000000000000000000000000000000
;; 219:000000dc999999dc11999dcc199999dd999919999999999e0000099900000000
;; 220:cdd50000dccd0000ddcd9999ddcd9991999999191999e9999999999900099999
;; 221:0191999001919990099199110999919900999919000999990000009900000000
;; 222:0009990000991900009919000099199000199990009199990009991900099919
;; 223:9999999999999919999999199999911991111999999999999999999999999999
;; 224:0000000000077000007007000700007007000070007007000007700000000000
;; 225:00000000111111111fff11111111111111111111111111911111191101111910
;; 226:00000000222222222fff22222222222222222222222222a222222a2202222a20
;; 227:00000000333333333fff33333333333333333333333333b333333b3303333b30
;; 228:00000000444444444fff44444444444444444444444444c444444c4404444c40
;; 231:cdd888dcdc8bb8dcdd8bb88cd8bbbb8d88bb3b808bb3b388bb33b3b8bb3bbb38
;; 232:000000dc000000dc00000dcc00000ddd00000000000000000000000000000000
;; 233:cdd50000dccd0000ddcd0000ddcdd00000000000000000000000000000000000
;; 234:00ddc0b00dcdd0bb0ccdddb30cddcb3b000003bb000033b5000b3b5b0003bb55
;; 235:9699999996999999999991999999119999919990911999009999900099900000
;; 236:099199990991999909911999009919e900991e99009999990009999100099199
;; 237:0000911900009919000009190000099900000099000000990000000e00000009
;; 238:9999991999199919999991199999119909999999000999990009996900009999
;; 239:999991990919999e000099990000000900000000000000000000000000000000
;; 240:0000000000000000000000000007700000077000000000000000000000000000
;; 241:000000000011110001f111101f11111111111111111111111111119101111910
;; 242:000000000022220002f222202f2222222222222222222222222222a202222a20
;; 243:000000000033330003f333303f3333333333333333333333333333b303333b30
;; 244:000000000044440004f444404f4444444444444444444444444444c404444c40
;; 247:000888dc008bb8dc008bb88c08bbbb8d88bb3b808bb3b388bb33b3b8bb3bbb38
;; 248:cdd88800dc8bb800dd8bb880d8bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 249:ddd3b555cc3355b3db3b55b3dbb55b3305555b33055bb33300bbbb3300b3bb3b
;; 250:00d888000d8bb8000c8bb88008bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 251:bb8dc000bb88d000bbb8dd003338cc00533b8000553b88085b3bb808553b388b
;; 252:999999999919e999991991e99911991909919999099199990999999909e9e999
;; 253:919991199999991999e1991199199991991e9911999e99919999999999999999
;; 254:9999991999999919999999199911999999e1999999e199999911999999199999
;; 255:199999e9919999999e99e9999999e99999e9199999199e999999911999999919
;; </TILES>

;; <TILES1>
;; 001:0011110001f881101f1881111889988118899881111881910118891000111100
;; 002:0022220002f882202f8aa82228aa8a8228a8aa82228aa8a202288a2000222200
;; 003:0033330003f383303f33b83333338b8338b83333338b33b303383b3000333300
;; 004:0044440004f844404f484444444cc884488cc444444484c404448c4000444400
;; 005:0077770007ff00707f0000077f00000770000087700000870700887000777700
;; 016:0000000000077000007007000700007007000070007007000007700000000000
;; 017:0011110001f881111f1881111889988118899881111881990118891100111100
;; 018:0022220002f882222f8aa82228aa8a8228a8aa82228aa8aa02288a2200222200
;; 019:0033330003ff83333f33b83333338b8338b83333338b33bb03383b3300333300
;; 020:0044440004f844444f484444444cc884488cc444444484cc04448c4400444400
;; 027:eeeeeeeeeeeeeeee8eeeeeee88eeeeee8888eeee8008888e8800088888888088
;; 028:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88
;; 029:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee0e8eeee0088eee808888ee0888888e
;; 030:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee88eeee88888ee8888888e
;; 031:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8eeeeeee8888eeee
;; 032:000770000070070007000070700ff007700ff007070000700070070000077000
;; 033:0011110001f881101f1881111889988118899881111881111118811191111119
;; 034:0022220002f882202f8aa82228aa8a8228a8aa82228aa82222288222a222222a
;; 035:0033330003f383303f33b8333f338b8338b83333338b333333383333b333333b
;; 036:0044440004f844404f4844444f4cc884488cc4444444844444448444c444444c
;; 043:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8eeeee888eee88888ee888888
;; 044:eeeee888eeee8888ee888880e888800088800000880000088000088888888888
;; 045:0888088e0808088e8008008e0088808e08888008888888088888888888888888
;; 046:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee8888e888880088800008
;; 047:eeeeeeeeeeeeeeeeeee88eeeee88888e8880088e800088880888888888888888
;; 048:0077770007000070700ff00770f00f0770f00f07700ff0070700007000777700
;; 049:0011110011f88110ff1881111889988118899881111881911118891000111100
;; 050:0022220022f88220ff8aa82228aa8a8228a8aa82228aa8a222288a2000222200
;; 051:0033330033f38330ff33b83333338b8338b83333338b33b33338bb3000333300
;; 052:0044440044f84440ff484444444cc884488cc444444484c444448c4000444400
;; 056:0009999999991111911116611166611666611191119999991111199900099999
;; 057:99999999999699ee99611e119111911111bb19119bbb1119b999999999999900
;; 058:fffffeeeffffffffffffffffffffffffffffffffffffffff66666fffffffffff
;; 059:eeeeeeeefeeeeeeeffffeeeefffffffefffffff6fffffffffffffff6fffffff6
;; 060:eeeeeeeeeeeeeeeee00eee00eee0e0eeeeee0eeeeeeeeeeeeeeeeeeeeeeeeeee
;; 061:eeeeeeeeeeeeeeeeeeeeffffefffffffee6fffffeee6666eeeeeeeeeeeeeeeee
;; 062:eeeeeeeeeeeeeeeeeeffffffffffffffffffffff66ffffffeee6666eeeeeeeee
;; 063:eeeeeeeeeeeeeeeeffffeefe666fffffffffffffffee666eeeeeeeeeeeeeeeee
;; 064:00ffff000f0000f0f0ffff0ff0f00f0ff0f00f0ff0ffff0f0f0000f000ffff00
;; 065:01f1111001f881101f1881111889988118899881111881910118891000111100
;; 066:02f2222002f882202f8aa82228aa8a8228a8aa82228aa8a202288a2000222200
;; 067:03f3333003f383303f33b83333338b8338b833b3338b33b303383b3000333300
;; 068:04f4444004f844404f484444444cc884488cc4c4444484c404448c4000444400
;; 074:ffffffffffffffffffffffffffffffffeee6666feeeeee66eeeeeeeeeeeeeeee
;; 075:fffffff6fffffff6fffffff6fffffff6ffffff666666666eeeeeeeeeeeeeeeee
;; 076:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 077:eeeeeeeeeeeeeeeeeeeeeeefeeeeffffee6fffffeeffffffefffffffffffffff
;; 078:eeeeeeeeeeeeeeeefffffeeeffffffeeffffff6effffff66ffffffffffffffff
;; 079:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee66eeeeeef66eeeee
;; 080:ff0000fff000000f00000000000000000000000000000000f000000fff0000ff
;; 081:0011110001f881111f1881111889988118899881111881910118891101111910
;; 082:0022220002f882222f8aa82228aa8a8228a8aa82228aa8a202288a2202222a20
;; 083:0033330003ff83333f33b83333338b8338b83333338b33b303383b3303333b30
;; 084:0044440004f844444f484444444cc884488cc444444484c404448c4404444c40
;; 088:eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 089:eeeeeeeeeeeeeeeeeeffffefeeffffffee6fffffeee66fffeeeee6ffeeeee66f
;; 090:eeeeeeeefeeeeeeeffeeeeeefffeeeeeffffeeeeffffeeeeffff6eeeff66eeee
;; 091:eeeeeeeeeeeeeeeeeeeeeeeeeeeeefffeeeeffffeeffffffeffffff6effffffe
;; 092:eeeeeeffeeefffffeeffffffeeffffffeeffffffeeffffffee6fffffee666fff
;; 093:ffffffffffffffffffffffffffffffffffffffffffffffffffffff66ff666666
;; 094:ffffffffffffffffffffffffff6fffffff6fffff66ffffff6fffffffffffffff
;; 095:ff66f6eefff6ffeeffffffeefffffffefffffffefffffffefffffffefffffff6
;; 097:0001100011188111fff881111889988118899881111889991118811100011000
;; 098:0002200022288222ff8aa82228aa8a8228a8aa82228aa8aa2228822200022000
;; 099:0003300033338333fff3b83333338b8338b83333338b3bbb3338333300033000
;; 100:0004400044484444fff84444444cc884488cc44444448ccc4444844400044000
;; 104:eeeeeeffeeeeefffeeefffffeeffffffee6ffff6eee6666eeeeeeeeeeeeeeeee
;; 105:ffeeeee6feeeeeeefeeeeeeefeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 106:66eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 107:e6fff6eeee666eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 108:eeee6666eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 109:66ffff66eeeeef6feeeee66feeeeee66eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 110:ffffffffffffffffffffffff6fffffff66666666eeeeeeeeeeeeeeeeeeeeeeee
;; 111:ffffff66fffff6eeffff66eeffff6eee6666eeeeeeeeeeeeeeeeeeeeeeeeeeee
;; 113:01f1111001f881111f1881111889988118899881111881910118891100111100
;; 114:02f2222002f882222f8aa82228aa8a8228a8aa82228aa8a202288a2200222200
;; 115:03f3333003f383333f33b83333338b8338b833b3338b33b303383b3300333300
;; 116:04f4444004f844444f484444444cc884488cc4c4444484c404448c4400444400
;; 120:9999999999991111911116611166666666611191119999991111199999199999
;; 121:9999999999979977997117119111911111551911955511195999999999999999
;; 122:00000000888888889999999911111111eeeeeeeee6e6e6e66e666e6e91996999
;; 123:667777766666666666667676666ee76761116ee76616666e6666676667777666
;; 124:eee11111911eeeee8991e11e88899999988888889988889899999989e1ee11ee
;; 125:eeeeeeee66666666116111161111111199999999919999999999999999999999
;; 126:eeeeeeee66666666111161111111111199999999999999999919999999991999
;; 127:eeeeeeee666eee66111666611111111199999999999199999999999999999999
;; 129:0011110011f88110ff1881111889988118899881111881911118891001111910
;; 130:0022220022f88220ff8aa82228aa8a8228a8aa82228aa8a222288a2002222a20
;; 131:0033330033f38330ff33b83333338b8338b83333338b33b333383b3003333b30
;; 132:0044440044f84440ff484444444cc884488cc444444484c444448c4004444c40
;; 136:66dddcd666cddcd666cdccd666cdcdd666ddddd666ddddd666cdcdd666ddddd6
;; 137:9991999909999999000991960000099900000099000000000000000000000000
;; 138:0999000099999999119991119991199991119919999999909900000000000000
;; 139:0000000099990000999999999119991199999999000991190000099900000000
;; 140:0000000000009999999999119111999999991199999999999099911900000000
;; 141:0000000099000000999990009199999099199999119919999999191999991919
;; 142:0000000000000000999000009999900019999900919199009919919009199190
;; 143:8888888866666666111111111111111199999999919999999999999999999999
;; 145:01f1111001f8811001f881101889988118899881011889100118891001111910
;; 146:02f2222002f88220028aa82028aa8a8228a8aa82028aa82002288a2002222a20
;; 147:03f3333003f3833003f3b83033338b8338b83333038b3b3003383b3003333b30
;; 148:04f4444004f8444004f84440444cc884488cc44404448c4004448c4004444c40
;; 155:9991990099199900991999009199900091990000999000006990000099000000
;; 156:9999990099199900991999009919900091190000999900009999900091999000
;; 157:0009999900099999009199990919991999999919991999990919999909999999
;; 158:0991999909919199099191990999999900999999001919990099199900099999
;; 159:8888888866666666011111110011111100099999000099990000099900000999
;; 161:011111101ff88110ff1881111889988118899881111881911118891000111100
;; 162:022222202ff88220ff8aa82228aa8a8228a8aa82228aa8a222288a2000222200
;; 163:033333303ff38330ff33b83333338b8338b83333338b33b333383b3000333300
;; 164:044444404ff84440ff484444444cc884488cc444444484c444448c4000444400
;; 167:000bbb33191bbb3313bbb3339bbbb3339bbb333bbbb3333b9b33bbbb9b333bdd
;; 168:3b5b3b803b35b389b3b5bb39b3355b31bb33533933b3bb8133b3b89933bbb899
;; 172:9199990091999900999199009991900099919000999190009991990099999900
;; 173:0191999001919990099199110999919900999919000999990000009900000000
;; 174:0009999900991999009919990099199900199999009199990009991900099919
;; 175:9999999999999999999999999999999999999999999999999999999999999999
;; 177:01f1111011f88110ff1881101889988018899880111889101118891001111110
;; 178:02f2222022f88220ff8aa82028aa8a8028a8aa80228aa82022288a2002222220
;; 179:03f3333033f38330ff33b83033338b8038b83330338bbb3033383b3003333330
;; 180:04f4444044f84440ff484440444cc880488cc44044448c4044448c4004444440
;; 183:3bb5bbb833b55b8bb3bb5b83b33b883bbbbb83bb000883b500883b5b0083bb55
;; 184:000000b0000000bb000000b300000b3b000003bb000033b5000b3b5b0003bb55
;; 185:bb800000bb880000bbb8000033380000533b8000553b88085b3bb808553b388b
;; 186:00088800008bb800008bb88008bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 187:9999919999919999999999999119991199999999000991190000099900000000
;; 188:0009999999999999999999119111999999991199999999999099911900000000
;; 189:99999999999999e99999e9999199999999199999119919999999191900091919
;; 190:0000000000000000999000009999900019999900919199009919919009199190
;; 191:99919999991199999999999999999999999999999991e99999e1999999999999
;; 192:0077770007ff00707f0000077f00000770000087700000870700887000777700
;; 193:01f1111011f88111ff1881111889988118899881111889911118811100000000
;; 194:02f2222022f88222ff8aa82228aa8a8228a8aa82228aa8a22228822200000000
;; 195:03f3333033f38333ff33b83333338b8338b83333338bbbb33338333300000000
;; 196:04f4444044f84444ff484444444cc884488cc44444448cc44444844400000000
;; 199:cdd88800dc8bb800dd8bb880d8bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 200:0003b555003355b30b3b55b30bb55b3305555b33055bb33300bbbb3300b3bb3b
;; 201:3b5b3b8b3b35b388b3b5bb38b3355b38bb33533833b3bb8833b3b8bb33bbb8bb
;; 202:b3bb5b88b3b55b88bbb5b5b88bb5b5b8bb5bb5b8bb5bb5b8b553bb58b5335b58
;; 203:0999000099999999119991119991199991119919999999909900000000000000
;; 204:0000000000000000000000000000000090000000999900009119900099999000
;; 205:0009999900099999009199990919991999999919991999990919999009999990
;; 206:9991999999919190699191906999990099999900991919009999199099999999
;; 207:999999999999119991119999999999999999999999999999999991999e999999
;; 208:00000000000ff00000f00f000f0700f00f0000f000f00f00000ff00000000000
;; 209:0111111001f88111011881110889988108899881011881910118891101111110
;; 210:0222222002f88222028aa82208aa8a8208a8aa82028aa8a202288a2202222220
;; 211:0333333003f38333033fb83303338b8308b83333038b33b303383b3303333330
;; 212:0444444004f8444404484444044cc884088cc444044484c404448c4404444440
;; 215:3bb5bbb833b55b8bb3bb5b83b33b883bbbbb83bb3bb883b53d883b5bdc83bb55
;; 216:000bbb33000bbb3303bbb3330bbbb3330bbb333bbbb3333b0b33bbbb0b333bdd
;; 217:333bbb803b35bbb85b3355b855b335b833bb355b3bbb3358bbbb3338cdd55338
;; 218:00ddc0000dcdd0000ccddd000cddcc0000000000000000000000000000000000
;; 219:000000dc999999dc11999dcc199999dd999919999999999e0000099900000000
;; 220:cdd50000dccd0000ddcd9999ddcd9991999999191999e9999999999900099999
;; 221:0191999001919990099199110999919900999919000999990000009900000000
;; 222:0009990000991900009919000099199000199990009199990009991900099919
;; 223:9999999999999919999999199999911991111999999999999999999999999999
;; 224:0000000000077000007007000700007007000070007007000007700000000000
;; 225:00000000111881111ff881111889988118899881111881911118891101111910
;; 226:00000000222882222f8aa82228aa8a8228a8aa82228aa8a222288a2202222a20
;; 227:00000000333383333fffb83333338b8338b83333338b33b333383b3303333b30
;; 228:00000000444844444ff84444444cc884488cc444444484c444448c4404444c40
;; 231:cdd888dcdc8bb8dcdd8bb88cd8bbbb8d88bb3b808bb3b388bb33b3b8bb3bbb38
;; 232:000000dc000000dc00000dcc00000ddd00000000000000000000000000000000
;; 233:cdd50000dccd0000ddcd0000ddcdd00000000000000000000000000000000000
;; 234:00ddc0b00dcdd0bb0ccdddb30cddcb3b000003bb000033b5000b3b5b0003bb55
;; 235:9699999996999999999991999999119999919990911999009999900099900000
;; 236:099199990991999909911999009919e900991e99009999990009999100099199
;; 237:0000911900009919000009190000099900000099000000990000000e00000009
;; 238:9999991999199919999991199999119909999999000999990009996900009999
;; 239:999991990919999e000099990000000900000000000000000000000000000000
;; 240:0000000000000000000000000007700000077000000000000000000000000000
;; 241:000000000011110001f881101f18811118899881188998811118819101111910
;; 242:000000000022220002f882202f8aa82228aa8a8228a8aa82228888a202222a20
;; 243:000000000033330003f383303f33b83333338b8338b83333338b33b303383b30
;; 244:000000000044440004f844404f484444444cc884488cc444444484c404444c40
;; 247:000888dc008bb8dc008bb88c08bbbb8d88bb3b808bb3b388bb33b3b8bb3bbb38
;; 248:cdd88800dc8bb800dd8bb880d8bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 249:ddd3b555cc3355b3db3b55b3dbb55b3305555b33055bb33300bbbb3300b3bb3b
;; 250:00d888000d8bb8000c8bb88008bbbb8088bb3b808bb3b388bb33b3b8bb3bbb38
;; 251:bb8dc000bb88d000bbb8dd003338cc00533b8000553b88085b3bb808553b388b
;; 252:999999999919e999991991e99911991909919999099199990999999909e9e999
;; 253:919991199999991999e1991199199991991e9911999e99919999999999999999
;; 254:9999991999999919999999199911999999e1999999e199999911999999199999
;; 255:199999e9919999999e99e9999999e99999e9199999199e999999911999999919
;; </TILES1>

;; <SPRITES>
;; 001:0000000000000000000000000000000000000000008888800880000888088888
;; 002:00000000000000000000000000000000088000008808888088800088c8c88888
;; 003:0000000000000000000000000000000000000000000000000000000080000000
;; 016:0000000800000008000000880000000800000008000000080000000800000008
;; 017:888088cc88088ccc888ccccc88cccccc8ccc87cc8cc787cccccccccccccccccc
;; 018:cccccc08cccccc08ccddcc00dddccc00d78ccc00d787cc00dccccc00dccccc00
;; 019:8000000000000000000000000000000000000000000000000000000000000000
;; 032:0000000800000008000000000000000000000000000000000000000000000000
;; 033:0cccccdd0ccccccc0ccccccc00ccccff000ccccc000ccccc00bbbbbc000b3bbb
;; 034:dcccc000ccccc000ccccc000cccc0000cccc0000cccc0000ccc00000bbbbb000
;; 048:00000000000000000000000000000bbb0000bbb300bb33bb0bbbb333bb333bb3
;; 049:00bb3333000b333300bb3333bbbb3333333b3333b3333333bb33333333333333
;; 050:b33bb000b333bbbbb33333b3b33933b33b33333b3b33333b3b3393333b333333
;; 051:00000000b0000000bb0000003bbb00003bbbb00033b33bb033b333bb3333333b
;; 065:00000000000000000000000800008888000888cc000888cc00888ccc0088cccc
;; 066:0000000000000000888800008888800088888800cc888880ccc88880cccc8888
;; 081:008ccccc088cc88c08cccccc00cc787c00cccccc00ccccdc00cccccd000ccccc
;; 082:cccc8888cc88c888ccccccc8c787ccd8ccccccc8ccccccc8dcccccd8cccccc08
;; 083:8000000080000000800000008000000080000000800000008000000080000000
;; 097:000cccff000ccccf0000cccc00000ccc0000000c0000000c0000cccc09999ccc
;; 098:ffcccc08fccccc08ccccc008ccccc088cccc0088cccc0088cccc0088ccccc988
;; 099:8000000080000000800000008000000080000000000000000000000080000000
;; 112:0000000000000009000000990000099100000991000009110000991100009111
;; 113:991199cc9111199c111111191111111111111111111111111111111111111111
;; 114:ccccc981cccc9981ccc911819c91118119111181111111111111111111111111
;; 115:8999000081190000881199001811199918111111111111111111111111111111
;; </SPRITES>

;; <MAP>
;; 018:858585858585858595a58585858585858585858585858585858585858585000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 019:8585c3858585858585858585b5858585c5d5a3b3858595a585c385858585000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 020:85b58585c385858585858585b6858585c6d6a4b4858696a685c4d4e4f485000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 021:85b68585c4d4e4f485d3f3e3f3858585858585858585858585c5d5e5f585000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 022:85858585c5d5e5f58585858585858585858585858585858585c6d6e6f685000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 023:85c1d185c6d6e6f68585858585858585858585858585d3e3f38585858585000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 024:f2c2d2b1e185e2f285858585858585e18585e2f2b1e1858585858585e2f2000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 025:8b9babf9f8d7f8f8d7e7f7e7d7e7f7f8e7d7f8f8f8f8d7e7d7d7e7f7f8f8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 026:8c9cac00feeefafafafafafafafafafafbfafffafbfffbfcfafafcfbfcfb000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 027:8d9dae9babab8b9bab98eefafafcdffafafafafafafcfcfdfdfaffeffaef000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 028:8e9e8c9cacac8c9cac000098fafaecfeeefdfafdfafafafaeffbfafafafb000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 029:83937a7dbfad8d9dad000000fefac9000000feeefffafafbfbfcfcfafafa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 030:8b9b7f9f8ac8bdcdc8d8000000e9ca00000000defbfbfbfafafbfafafafa000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 031:8c9cac8d9d8b9bab00dc000000eac90000000000cffafffdfafafafdfbfd000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 032:8d7dbf7f9e8c9cac00eed8cc00d9b90000000000cefceffffffbfafffbff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 033:8e9f9cac008d9dad0000febbcbbe000000000000defbfffcfafffcdffaff000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 034:008d9dad0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </MAP>

;; <WAVES>
;; 000:0000000000ffffff0000000000ffffff
;; 001:0123456789abcdeffedcba9876543210
;; 002:0123456789abcdef0123456789abcdef
;; 003:10f000500080000cf000c000f7004010
;; 004:0479ceffecdefffedb85345677643210
;; </WAVES>

;; <SFX>
;; 000:e00070003000200010001000100010002000300030004000500050005000500050005000500050005000500050005000500050005000500050005000304000000000
;; 001:44000400040004000400240024002400440044005400540054005400640064006400740074007400840094009400a400a400b400c400d400d400e400270000000000
;; 002:020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200000000000000
;; 003:0300030003000300030003005300630063006300630063007300830083009300b300c300c300e300f300f300f300e300f300e300e300f300f300f300010000000000
;; 004:140044009400c400e400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400f400109000000000
;; 005:050045008500a500b500d500e500e500e500e500e500e500e500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500205000000000
;; 006:f0085009700eb003e007f000f000e000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000f000405000000000
;; 007:f500c5009500b500d500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500f500507000000000
;; 008:00000000000000000040004010401040107020702070207030b030b030b030b040405040704080409070a070a070b070b0b0c0b0d0b0e0b0f070f070410000000000
;; 009:f1088109410911090109010a010a010b110b110c110c210c310c310d410e510f610f71007101810281029103a103b104b105c106c106c106e107f10732b000000000
;; 010:f000d000b0009000700060005000500050006000600060006000700070007000800080008000900090009000a000b000c000c000d000d000d000d000309000000000
;; </SFX>

;; <PATTERNS>
;; 000:400030b0004c400056400030400030b0004a400056b0004a400030b0004c400056400030400030b0004a400056b0004a400030b0004c400056400030400030b0004a400056b0004a400030b0004c400056400030400030b0004a400056b0004a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 001:400016000000b00014400016100000400016b00014000000900014000000000000900014900014000000800014000000600014000000000000600014000000000000600014000000b00014000000000000b00014000000000000d00014f00014000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 002:40001c00000010000040001c10000060001c80001c90001c100000d0001a100000d0001a000000d0001ab0001a00000000000010000080001a00000090001a80001a60001a00000060001a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 003:80001c00000010000080001c10000090001cb0001cd0001c10000040001c10000040001c60001c80001c60001c40001c000000100000b0001a000000d0001ab0001ab0001a000000b0001a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 004:800006000000000000000000800006000000600006000000400006000000000000000000400006100000400006100000400006100000400006400006100000400006100000400006800006100000600006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 005:b00004000000000000000000b00004000000f00004000000d00004000000000000000000d00004100000d00004100000d00004100000d00004d00004100000d00004100000d00004b00004100000f00004000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 006:800006000000000000000000800006900006b00006d00006000000000000000000000000d00006000000400008000000d00006000000000000000000d00006f00006400008f00006000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 007:b00004000000000000000000b00004600004400004900004000000000000000000000000900004000000800004000000600004000000000000000000800004900004d00004b00004000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 008:932616000000000000000000000000000000432616632616b32616000000000000000000000000000000432616632616d32616000000000000000000000000000000432616632616b32616000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 009:000000000000d326a6000000d326a60000a0b326a6d000a6b326a69326a60000a09326a60000a00000000000a00000009326a60000009326a60000009326a66326a64326a60000004326a66326a69326a6b326a60000a00000000000a0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 010:400030000000000000000000b0005a000000000030400030400030000000000000000000b0005a000000000000400030400030000000000000000000b0005a000000000000400030400030000000000000000000b0005a000000000000400030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 011:0000000000004326a80000000000000000006326a89326a86326a8d326a8000000000000000000000000000000000000d326a80000000000000000004326aa000000000000000000b326a80000000000006326a86326a84326a8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 012:832616000000000000000000000000000000a32616000000000010000000000000000000532616000000000000000000000000000000000000000000000000000000000000000000532616000000000000000000000000000000d32614000000000000000000000000000000c32614000000000000000000000000000000532614000000000000532614000000000000532614000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 013:000000000000000000000000f32616000000f32616000000000000000000d32616000000c32616000000000000000000000000000000532618f32616532618000000000000000000000000000000000000832616000000000000a32616000000000000c32616000000000000f32616000000000000000000000000000000532618832618a32618532618000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; 014:488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000488130000000488158000000a8815a000000400030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
;; </PATTERNS>

;; <TRACKS>
;; 000:1800001800001805811807021805811807021803011803010000000000000000000000000000000000000000000000007e0240
;; 001:982cc2982cc2000000000000000000000000000000000000000000000000000000000000000000000000000000000000cc0240
;; 002:d83f00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a04040
;; </TRACKS>

;; <PALETTE>
;; 000:1a1c2c3b5dc9b13e5338b764ffcd7518ca2095c2e2c6ceba2c28442838aa7528302c7938ba996da5694c00a1d2ffffff
;; </PALETTE>

;; <PALETTE1>
;; 000:1a1c2c3b5dc9b13e5338b764ffcd7500000095c2e2c6ceba2c28442838aa7528302c7938ba996da5694c00a1d2ffffff
;; </PALETTE1>

